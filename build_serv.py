import pyrax
import os
import time
import ssl
import requests


# Autheicate
pyrax.set_setting("identity_type", "rackspace")
creds_file = os.path.expanduser("~/.auth.cfg")
pyrax.set_credential_file(creds_file, region="IAD")

# create alias for cloud servers
cs = pyrax.cloudservers

# create variable for server name
server_name = "L2.candidate4.web1"
server_name2 = "L2.candidate4.web2"

# create variable for image name
cent_img = "c25f1ae0-30b3-4012-8ca6-5ecfcf05c965"

# create variable for falovr
cent_flv = "general1-1"

# create the server
server = cs.servers.create(server_name, cent_img, cent_flv)

# create second server
server2 = cs.servers.create(server_name2, cent_img, cent_flv)

# wait for server to build
# yes im know im being lazy
print "waiting 2 minutes for server to build"
time.sleep(120)

# request info on the created server
print("Name:", server.name)
print("ID:", server.id)
print("Status:", server.status)
print("Admin Password:", server.adminPass)
print("Networks:", server.networks)

# request info on the created server2
print("Name:", server2.name)
print("ID:", server2.id)
print("Status:", server2.status)
print("Admin Password:", server2.adminPass)
print("Networks:", server2.networks)
