import pyrax
import os
import time
import ssl
import requests


# Autheicate
pyrax.set_setting("identity_type", "rackspace")
creds_file = os.path.expanduser("~/.auth.cfg")
pyrax.set_credential_file(creds_file, region="IAD")

# create vairable to call for pyrax load balancers
clb = pyrax.cloud_loadbalancers

# create variable for load balancer name
lb_name = "L2.candidate4.http"

# create node for L2.candidate4.web1
node1 = clb.Node(address="10.209.134.28", port=80, condition="ENABLED")

# create node for L2.candidate4.web2
node2 = clb.Node(address="10.209.134.114", port=80, condition="ENABLED")

# create public vip
vip = clb.VirtualIP(type="PUBLIC")

# create the load balncer
lb = clb.create(lb_name, port=80, protocol="HTTP", nodes=[node1, node2], virtual_ips=[vip])

print "Load Balancer:", lb.name
print "ID:", lb.id
print "Status:", lb.status
print "Nodes:", lb.nodes
print "Virtual IPs:", lb.virtual_ips
print "Algorithm:", lb.algorithm
print "Protocol:", lb.protocol
