# PyraxScripts

## Info

these scripts were made to automate the building of rackspace cloud servers and load balancers.  they were written for a specific purpose.

## About .auth.cfg

.auth.cfg is the authentication file.  You will not find my username and api key however if you plan to use this script with rackspace cloud you will need to provide this inforamtion.

### build_serv.py

build_serv.py will build 2 Rackspace General purpose 1 GB Centos 7 servers in the IAD datacenter.  

### build_lb_http.py

build_lb_http.py will create an http load balancer and add 2 nodes with the given service net ip address.

### build_lb_https.py

build_lb_https.py will create an https load balancer and add 1 node with the given service net ip address.

